from bs4 import BeautifulSoup
from nltk.tag import pos_tag
import re
import spacy
import pandas as pd

nlp = spacy.load("en_core_web_trf")

def next_word(target, source):
    for i, w in enumerate(source):
        if w == target:
            return source[i+1]
        
def nlp_core(text):
    found_list = []
    
    text = text.replace('?', ' ')
    tagged_sent = pos_tag(text.split())
    
    #propernouns = [word for word, pos in tagged_sent if pos == 'NNP']
    propernouns = []
    for word, pos in tagged_sent:
        #print(word, pos)
        if "http" in word:
            continue
        if re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[a-zA-Z]+$', word):
            last_number = str(re.findall(r'\d+', word)[-1])
            position = word.rfind(last_number) + len(last_number)
            word = word[position:]
            pos = (pos_tag([word])[0])[1]
            #print(pos)
        if (pos == 'NN' or pos == 'NNS' or pos == 'NNPS' or pos == 'NNP'):
            propernouns.append(word)
                
    # print("All Kinds of Nouns ------------------------------------------------")        
    # print(propernouns)
    
    # filterednouns = []
    # for noun in propernouns:
    #     about = nlp(noun)
    #     #print(noun, about.ents)
    #     if len(about.ents) >= 1:
    #         filterednouns.append(noun)
        
    # print("What I think are proper nouns ------------------------------------")   
    # print(filterednouns)
    
    compound_nouns = []
    for noun in propernouns:
        regex = r'(?<=' + noun + '\s)\w+'
        following_words = re.finditer(regex, text)     

        for word in following_words:
            cwords = noun + ' ' + word.group(0)
            if len(re.findall(r'[hH][1-5]', cwords)) > 0 or len(re.findall(r'Modified [1-9]', cwords)):
                # print('Tossing HTMLism {}'.format(cwords)) 
                pass
            elif cwords not in compound_nouns:
                compound_nouns.append(cwords)
            
    #print("Found Compound Nouns --------------------------------------------")   
    #print(compound_nouns)
            
    products = [] 
    for cnoun in compound_nouns:
        doc = nlp(cnoun)
        
        # if len(doc.ents) == 0:
        #     print(cnoun, '-----', doc.ents)
        
        for ent in doc.ents:
            #print(cnoun, ent.label_)
            #print(pos_tag([cnoun]))
            if ent.label_ == 'PRODUCT' or ent.label_ == 'CARDINAL' or ent.label_ == 'ORG':
                if cnoun not in products and 'Microsoft' not in cnoun:
                    products.append(cnoun)
            
    # print("These are Products ---------------------------------------------")   
    # print(products)
    # print('\n')
    
    found_list = products
    return found_list

def find_proper_nouns_in_html_file(htmlfile):
    with open(htmlfile, 'r') as f:
        contents = f.read()
        soup = BeautifulSoup(contents, 'html.parser') 
        text = (soup.get_text().encode("ascii", "replace")).decode()
        return nlp_core(text)


def find_proper_nouns_in_html(html):
    soup = BeautifulSoup(html, 'html.parser') 
    text = (soup.get_text().encode("ascii", "replace")).decode()
    return nlp_core(text)


def find_proper_nouns_in_text_file(textfile):
    with open(textfile, 'r') as f:
        text = f.read()
        return nlp_core(text)


def find_proper_nouns_in_text(text):
    return nlp_core(text)


# fin